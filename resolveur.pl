% zone noire
aire(X, Y, X, Y, 1) :- case(X, Y, 1).

% zone blanche
aire(X1, Y1, X2, Y2, 0, T) :- X1 \= X2, case(X1, Y1, 0), NX is X1 + 1, aire(NX, Y1, X2, Y2, 0, T).
aire(X, Y1, X, Y2, 0, T) :- Y1 \= Y2, case(X, Y1, 0), NY is Y1 + 1, airereverse(X, NY, X, Y2, 0, T).

% zone : E = Etat, T = Taille
aire(X1, Y1, X2, Y2, E, T) :- E \= 0, write("\n"), write(X1), write(" "), write(Y1),
                                      write(" "), write(X2), write(" "), write(Y2),
                                      write(" "), write(E), write(" "), write(T).

airereverse(X1, Y1, X2, Y2, 0, T) :- case(X1, Y1, 0), NX is X1 - 1, NX \= T,
                                     airereverse(NX, Y1, X2, Y2, 0, T);
                                     NX = T, aire(NX, Y1, X2, Y2, 0, T).

% cellule
case(X, Y, C) :- write("\n"), write("Coordonnées: X="), write(X),
                write(", Y="), write(Y),
                write(" | Couleur: "), write(C).

run([]) :- write('0: case blanc, 1: case noire').
run([[X1, Y1, X2, Y2, E] | Q]) :- T is X2 - X1, aire(X1, Y1, X2, Y2, E, T), run(Q).

/*
run([[0,0,1,1,2],
     [2,0,2,2,-1],
     [3,0,5,1,-1],
     [0,2,0,4,-1],
     [1,2,1,3,0],
     [3,2,5,2,0],
     [1,4,1,4,1],
     [2,3,5,5,4],
     [0,5,1,5,-1]]).
*/
